In the previous programs we've written, there hasn't been a lot of structure in regards to how the data "looked." When we had pieces of data, any changes we might make that were logically related were left up to us to do, manually. A great example of this is the course schedule example that we used throughout the <a href="lists/1/">lists lesson</a>. Every time we wanted to add a new class, we had to first add the name of the course to <python>schedule</python>, and then we had to add the grade to <python>grades</python>. It was up to us to make sure that the data was in order, and we had to do it all manually.

But, we don't have to. What if we were, instead, to write functions that would maintain a certain structure in our data, data that was related logically? Imagine that I told you that you had to:
<ol>
<li>Drop the second course in the schedule.</li>
<li>Change the grade for the first course in schedule to 61.7.</li>
<li>Add a new course "Advanced Japanese 2" as the fourth course in schedule.</li>
<li>Drop the fifth course in the schedule.</li>
<li>Add a new course "Linguistics 101" as the last course in schedule.</li>
<li>Change the grade for "Linguistics 101" to 97.5.</li>
<li>Print out how many courses had grades of 75% or lower.</li>
<li>Give me a list of the names of the courses with grades of 75% or lower.</li>
</ol>

I think it's easy to see that doing this would be very tedious, and it'd be very easy for us to make mistakes in about a hundred places along the way. What's more is that we'd be required to manually preserve the data structure at every point along the way &mdash; and we'd be responsible for knowing that when we're asked to change the grade for "Linguistics 101" to 97.5, what that means!

But there's another way of doing all of this &mdash; objects. There are about as many types of "objects" in computer programming as there are students in this class (and that number is being conservative, I'm sure); we're only going to look at a few of these &mdash; the major ones &mdash; over the rest of this course. For this lesson, we'll look at the first of these types.

Using the objects as we describe them in this lesson, we can simplify the list of requests from above into something easy:
<code>dropCourse(1)
changeGrade(0, 61.7)
addCourse(3, "Advanced Japanese 2")
dropCourse(4)
addCourse("Linguistics 101")
changeGrade("Linguistics 101", 97.5)
printGradesLessThanOrEqual(75)
courseNames = getCoursesGradeLessThanOrEqual(75)</code>

And that'd be all we'd need to do, if we use objects. Much easier, right? Well, let's learn <emphasis>how</emphasis>.