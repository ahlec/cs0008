numbers = []
while True:
    print("There are %d numbers." % len(numbers))
    number = int(input("Add a number (-1 to quit): "))
    if number == -1:
        break

    havePutNumber = False
    for position in range(len(numbers)):
        if numbers[position] == number:
            havePutNumber = True
            break
        if numbers[position] > number:
            # we want to put number before this position
            numbers[position:position] = [number]
            havePutNumber = True
            break

    if havePutNumber == False:
        numbers = numbers + [number]
    
print("")
print("Sorted numbers:")
for number in numbers:
    print(number)
