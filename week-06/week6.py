def printSchedule(schedule, grades):
    divider = ("-" * 48)[:47]
    divider = divider[0:6] + "+" + divider[7:]
    print(divider)
    print("%6s|%40s" % ("Grade", "Course"))
    print(divider)
    for index in range(len(schedule)):
        print("%5.1f%%|%40s" % (grades[index], schedule[index]))
    print("")

def printShoppingList(itemsNeeded, numberNeeded):
    divider = ("-" * 48)[:47]
    divider = divider[0:30] + "+" + divider[31:]
    print(divider)
    print("%30s|%16s" % ("Item", "Quantity"))
    print(divider)
    for index in range(len(itemsNeeded)):
        print("%30s|%16d" % (itemsNeeded[index], numberNeeded[index]))
    print("")
