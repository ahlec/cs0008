Well, how about adding to a list &mdash; but not to the end of it; to the middle? That's where subsets become interesting (and helpful).

In the past example, we replaced a non-empty subset from our list with a new, empty list. That "deleted" from the list. But if we replaced an empty subset from our list with a new, non-empty list? We'd be "creating" something in our list where nothing existed before!

Now we just have the "trivial" task of talking about something that "doesn't exist."

Remember how, like <python>range()</python>, the end of the range isn't included? When we used <python>alphabet[2:4] = ["X", "Y"]</python>, we only "changed" <python>alphabet[2]</python> and <python>alphabet[3]</python>. We can essentially say, then, that when we write <python>someList[START:END]</python>, the length of the subset of someList that we're asking for is END - START. By that notion, then, if START and END were the same number, the length of the subset would be 0 &mdash; an empty list.

If we talk about <python>someList[1:1]</python>, we're talking about an empty subset that begins at index 1 and ends at index 1. And if we replace this, then suddenly the list which begins and ends at 1 will have something in it. We added something.

Let's take a look back at our course schedule example now to cement this idea:
<code>schedule[2:2] = ["Computational Methods in the Humanities"]
grades[2:2] = [99.6]
printSchedule(schedule, grades) 

>> ------+----------------------------------------
>>  Grade|                                  Course
>> ------+----------------------------------------
>>  95.7%| Introduction to programming with Python
>>  93.0%|                  Logic and Intelligence
>>  99.6%| Computational Methods in the Humanities
>>  83.1%|                             Chemistry 2
>>  80.1%|                              Calculus 1</code>

As you can see, we added the new class "Computational Methods in the Humanities" at index 2.

<note>Because the length of the replacement list doesn't need to match the length of the subset list to be replaced, we aren't limited to just inserting one element at a time; if you want to insert multiple consecutive elements into a list, that's possible as well.</note>

There are many other things you can do with lists as well that aren't covered here &mdash; once you understand the basics covered here on using lists, experiment and see what else you can do!