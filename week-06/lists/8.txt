So how do subsets help us to delete things?

Remember how we could change the value of an element of our list by indexing it?
<code>schedule[1] = "Introduction to Painting"</code>

What if we were, instead of changing the element itself (in this instance, the string <python>"Introduction to Painting"</python>), to change instead the <emphasis>list</emphasis> containing the element? What if we were to change the <emphasis>subset list</emphasis> that contains the element, and we replaced <emphasis>that</emphasis> list with a <emphasis>new</emphasis> list?

Well, we'd replace that subset list!
<code>alphabet = ["A", "B", "C", "D", "E", "F"]
for letter in alphabet:
    print(letter)
print("---------------")
alphabet[2:4] = ["X", "Y"]
for letter in alphabet:
    print(letter)
    
>> A
>> B
>> C
>> D
>> E
>> F
>> ---------------
>> A
>> B
>> X
>> Y
>> E
>> F</code>

See how we replaced the subset list that consisted of indeces 2 (<python>"C"</python>) and 3 (<python>"D"</python>) with a new list (<python>["X", "Y"]</python>)?

But when we're changing a subset of a list like this, the subset we're changing and the replacement list don't need to be the same length. In fact, they don't even need to have anything at all! That's how we delete something &mdash; we <emphasis>replace</emphasis> the subset list that contains the element(s) we want to remove with an <emphasis>empty list</emphasis>.

So, let's drop the Introduction to Painting course.
<code>schedule[1:2] = []
grades[1:2] = []
printSchedule(schedule, grades)

>> ------+----------------------------------------
>>  Grade|                                  Course
>> ------+----------------------------------------
>>  95.7%| Introduction to programming with Python
>>  93.0%|                  Logic and Intelligence
>>  83.1%|                             Chemistry 2
>>  80.1%|                              Calculus 1</code>

<note>Because we're using parallel lists here, we can't forget to modify both lists &mdash; otherwise, our data will become corrupt.</note>