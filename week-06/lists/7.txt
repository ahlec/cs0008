So we can now easily remove things from the end of a list, but what about from anywhere else? How do we remove, say, the second item in a list?

In order to do that, we first need to understand a new twist to a familiar piece of syntax:
<code>schedule[1:3]</code>

This looks somewhat familiar, right? It almost looks like this:
<code>schedule[1]</code>
But that colon and second number &mdash; what are they suddenly doing?

Well, when we wrote schedule[1], we were getting a single element from a list; we were retrieving or modifying the second element of the list <python>schedule</python>. When we write <python>schedule[1:3]</python>, we're actually retrieving a <a href="http://simple.wikipedia.org/wiki/Subset" target="_blank" class="term">subset</a> of our list. Instead of retrieving a single element, we're retrieving a consecutive range of elements.

<note>This consecutive range of elements from our list is itself a list &mdash; just a smaller one.</note> 

Let's look at an example to cement this concept:
<code>alphabet = ["A", "B", "C", "D", "E", "F"]
for letter in alphabet:
    print(letter)
print("--------------")
for letter in alphabet[1:4]:
    print(letter)
    
>> A
>> B
>> C
>> D
>> E
>> F
>> --------------
>> B
>> C
>> D</code>

The first loop prints every letter in the list; but the second loop will print only the elements from index 1 to index 3. The "end point" of this range is actually non-inclusive, just like the <python>range()</python> function that we're familiar with!