For this week, I put together a special <strong>library</strong> designed for the class schedule examples we're using for this list section: <a href="files/week6.py" target="_blank">week6.py</a>. There's only one function in here that we are using, though &mdash; printSchedule(). To save you time and not need to worry about the (<emphasis>now</emphasis) mundane task of printing out data, you can include the library and use it to print the schedule for you!

To use this library, save the file to the same directory as the code you're using as you follow along. Make sure to save this file with the default filename ("week6.py"). Once you've done this, we include the library just as we have other libraries:

<code>from week6 import *</code>

Once this is done, you can call a new function in your code: printSchedule. The first argument to printSchedule will be the list of course names, and the second argument will be the list of grades. If you haven't changed either of the variable names, we can then put this in our code:

<code>printSchedule(schedule, grades)</code>
And then get a nice, pretty table!

<code>>> ------+----------------------------------------
>>  Grade|                                  Course
>> ------+----------------------------------------
>>  95.7%| Introduction to programming with Python
>>  88.2%|                Introduction to Painting
>>  93.0%|                  Logic and Intelligence
>>  83.1%|                             Chemistry 2
>>  80.1%|                              Calculus 1</code>

Simplifies matters a bit, doesn't it?

<note>You'll notice that, if you use this, you'll suddenly have a folder named <strong>__pycache__</strong>. This is just something that Python does when you include a file. When you're all done with this, nothing bad will happen if you delete this folder.</note>