# CS0008 Recitation, Week 04: Additional Problem
#
# PROBLEM:
#   Let's use turtle graphics to draw a *filled* Triforce (see: The Legend of
#   Zelda).
# RULES:
#   *   You must have a function that will draw a single filled triangle,
#       which you  will then call a total of three times to draw your Triforce.
#   *   Your code must work for an arbitrary side length.
#   *   You cannot explicitly set the position of your turtle -- you must walk
#       him every step of the way.
# SOME HINTS:
#   *   How do we fill in something with the turtle? Well, the steps are rather
#       simple, actually:
#           1.) Tell the turtle that we are going to fill in whatever we draw.
#           2.) We draw the shape that we want to fill.
#           3.) Tell the turtle that we are done drawing the shape.
#       And once we finish step 3, the turtle will fill in what he drew! So, how
#       do we tell the turtle how to fill something in? Well, to tell him that
#       we are going to fill in a shape, we use:
#
#           turtle.begin_fill()
#
#       and then to tell him that we are done drawing the shape and he should
#       fill in what he just drew, we use:
#
#           turtle.end_fill()
#   *   Once the program is working, the side length won't change. Therefore,
#       it is allowable that you have a variable with the side length within
#       your program. It's only required that it be *possible* to change the
#       side length. Meaning, you can have some variable "sideLength" in your
#       code, but you are not allowed to have any statements that look like
#       "turtle.forward(150)"
#           * NOTE: This applies only to forward() / back() statements, not to
#             left() / right() statements.
#   *   At the end of your program, put either "turtle.Screen().waitforclick()"
#       or "turtle.done()" to make it easy to close out of your program.
import turtle

def fillTriangle(sideLength):
    turtle.begin_fill()
    for side in range(3):
        turtle.forward(sideLength)
        turtle.left(120)
    turtle.end_fill()


fillTriangle(150)
turtle.forward(150)
fillTriangle(150)
turtle.left(180)
turtle.forward(150)
turtle.right(120)
turtle.forward(150)
turtle.right(60)
fillTriangle(150)
turtle.done()