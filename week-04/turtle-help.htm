<a href=".">Return to directory</a>

<h1>Turtle graphics: help and hints</h1>

Below are a couple of guidelines that I've put together, to make using
<a href="http://docs.python.org/2/library/turtle.html">turtle graphics</a> in python easier.

<ol>
    <li><b>How do I let the program exit?</b><br />Having the turtle graphics window close when you are done with drawing
    can be an annoying task to accomplish, and Windows and IDLE aren't particularly kind about it. However, <i>we</i> as
    programmers can make this happen, by adding the following line of code to our program:<br /><br /><code>turtle.Screen().exitonclick()</code><br /><Br />
    So what is it doing? Well, let's take a look at it: first, we have "<b>turtle</b>" which is just telling Python where
    it should look to find whatever else we give it next &mdash; the same way when we write <b>math.sqrt</b>, how we are
    telling Python that we're looking for the square root (sqrt) function <i>within the math library</i>. Next, we have
    <b>"Screen()"</b> &mdash; <u>which is a function</u>. But what <i>is</i> it? Well, when we call turtle.Screen(), it will
    return to us the, well, screen that the turtle graphics are being drawn upon. There is a specific reason why this is
    a function, but it has to do with some more higher-level programming concepts that we aren't going to be focusing on
    right now<sup>1</sup>. All we really need to know is that we can be using <b>turtle.Screen()</b> as a variable, because
    it will always give us the same screen. Finally, we have <b>exitonclick()</b>, the thing we're <i>really</i> focused on.
    This is a function of the screen that tells the screen "when the user clicks on you, close yourself." We'll return one
    day to what it means when I say it is "a function of the screen,<sup>2</sup>" but for now, it's acceptable to just use the code
    as I put it above.</li>
    <li><b>Where do I put <code>turtle.Screen().exitonclick()</code>?</b><br />Well, anywhere you want this to happen!
    However, wherever we put this in the code, it will pause the program and wait for the user to click it and close
    it. And if we do more things with the turtle (such as moving or turning), then the window will show back up! Only this
    time, it won't close when we click on it. So the best place to put it would be at the end of your program, when you're
    all finished &mdash; though that certainly isn't the only place it's allowed to go.</li>
    <li><b>I know what I want to draw &mdash; the shape, I mean &mdash; but how do I go about determining how to draw it?</b><br />
    This, more than anything, is a conceptual problem. It all boils down to: "how would I go about drawing this with pen
    and paper?" That is, imagine a scenario where you need to draw, say, a house, but you cannot lift your pen while you're
    drawing it. Well, first, I'd imagine that I would draw a rectangle &mdash; the house itself. I'd draw a line, then I'd
    turn; draw a line, turn; draw a line, turn; draw a line. I've drawn a box without lifting my pen once. So what was I 
    doing while drawing it? Well, if we <i>really</i> break it down, we'd realize that at each step, I'd draw a line, then
    look down at my pen and say "okay, my pen is right here, and I just drew this straight line to get here. Now, I want to
    draw a new side, so I need to change what direction my pen is drawing in." But at all times, I'm keeping track of where
    my pen is on the paper &mdash; if I didn't know where my pen was at, or <i>what direction it had just been drawing in</i>,
    it'd make it much more difficult to draw a good rectangle. But that's just it &mdash; when I set out to draw the rectangle,
    I kept in mind that I would draw a straight line, and then I would turn my pen; therefore, when I was ready to draw
    a line, I knew that I was already at the end of a line, and all I needed to worry about was drawing a straight line
    forward and turning once I was done. It didn't matter, then, what side I was on, because I knew what rules I was drawing
    by.<br /><br />
    tl;dr: Draw it out on paper, or in your mind. Figure out what steps you were following in your head, and always keep
    track of what direction you were going. When drawing multiple, different shapes, if you know where and what direction
    drawing the previous shape left you at, it'll be easier to coordinate.</li>
    <li><b>How do I know what direction to do things in?</b><br />Remember that, for geometric shapes, the sum of the
    external angles is 360&deg;. Think about how the turtle moves, then. It will turn left() or right() by some degree &mdash;
    but it turns left() or right() by some degree <i>from the horizontal plane it was moving on</i>. So how is this helpful?
    Well, because this all equates to the idea that we're <i>drawing shapes from the outside</i>, meaning that, if we draw a
    full, connected shape, the sum of all angles and turns that we made should be 360&deg;. For instance, if we're drawing a
    connected box, we would: <ol><li>Draw a straight line (let's say, the top).</li><li>Turn 90&deg; to the right (so we're now
    pointing straight down).</li><li>Draw a straight line (the right side).</li><li>Turn 90&deg; to the right (so we're now facing
    to the left).</li><li>Draw a straight line (the bottom).</li><li>Turn 90&deg; to the right (so we're now facing up).</li>
    <li>Draw a straight line (the left).</li></ol>Well, at each turn, we made 90&deg; turns. 90&deg; * 4 = 360&deg;. It works out
    like this.</li>
    <li><b>Can I simplify things?</b><br />Absolutely! Don't forget your <code>for ... in range(...)</code> loops ;)</li>
</ol>

<hr />
<sup>1</sup> If you're really curious, the concept here is known as a <a href="http://en.wikipedia.org/wiki/Singleton_pattern"
target="_blank">Singleton</a>. In a simplification of the concept, the first time the function is called, it creates
and stores a copy of the object it was asked for; every time afterwards that the function is called, it will return
the copy of the object it made, so that the function will <i>always</i> be returning the same piece of information, but
it doesn't matter where that object was created &mdash; or when. Singletons are very powerful things, but they're beyond
the scope of this class.<br /><br />
<sup>2</sup> The concept here deals with <a href="http://en.wikipedia.org/wiki/Object-oriented_programming"
target="_blank">object-oriented programming</a> and <a href="http://en.wikipedia.org/wiki/Class_(computer_programming)"
target="_blank">classes</a>, something that will be covered later in this course, but which aren't necessary to know
right now. Just put this here in case you were curious.