# Originally this was going to be an exercise, but I decided that it contained
# a number of things that hadn't been covered in covered in class yet, and that
# this code might be helpful for exploration of the turtle graphics library,
# and for determining how to draw something in turtle
#
# So what does this do? Well, it will open up turtle, and allow you to then
# use the arrow keys to control the turtle. However, it works with snake
# controls (when you are facing left, hitting Down will automatically make you
# move down).

import turtle

turtleIsMoving = False # We have a variable here for whether the turtle is
                       # moving right now or not; by having this, we can make
                       # sure that the turtle will not malfunction and go
                       # left and right at the same time, for instance.
def moveUp():
    global turtleIsMoving # But, we need to be able to use our variable within
                          # our function. We can't pass turtleIsMoving into the
                          # function, because we don't have control over
                          # calling moveUp (as you'll see below). Plus, we want
                          # to be able to modify it. Therefore, at the top of
                          # our function, we will say "global turtleIsMoving"
                          # while tells Python that when we say "turtleIsMoving"
                          # in our moveUp() function, we really mean the
                          # variable turtleIsMoving that we created *outside*
                          # of the program. NOTE: Don't make common use of this.
                          # Use arguments whenever you can, and unless Dr.
                          # Novacky teaches this in lecture, certainly don't
                          # use this on the exams :P
    if turtleIsMoving:
        return # if turtleIsMoving is True, then we know that, somewhere else
               # one of the arrow keys is already moving the turtle. So, we
               # want to just exit out of the function. We use "return" here to
               # exit out of our function prematurely.
    turtleIsMoving = True # We're about to move the turtle, so let's set
                          # turtleIsMoving to True so that no one else will move
                          # the turtle
    turtle.setheading(90)
    turtle.forward(10)
    turtleIsMoving = False # We finished moving the turtle, so we need to set
                           # turtleIsMoving back to False so that another
                           # function can move the turtle. And then we're done
def moveDown():
    global turtleIsMoving
    if turtleIsMoving:
        return
    turtleIsMoving = True
    turtle.setheading(270)
    turtle.forward(10)
    turtleIsMoving = False
def moveLeft():
    global turtleIsMoving
    if turtleIsMoving:
        return
    turtleIsMoving = True
    turtle.setheading(180)
    turtle.forward(10)
    turtleIsMoving = False
def moveRight():
    global turtleIsMoving
    if turtleIsMoving:
        return
    turtleIsMoving = True
    turtle.setheading(0)
    turtle.forward(10)
    turtleIsMoving = False

screen = turtle.Screen() # We're going to be using a number of functions that
                         # deal with the screen. Because I was lazy and didn't
                         # want to write out "turtle.Screen()" a number of times,
                         # I'm going to save the value we get when we call
                         # turtle.Screen(), into a variable "screen" so that
                         # all I have to do is type "screen" now
screen.onkey(moveUp, "Up") # The Turtle Screen has the ability to keep track of
                           # things called "events" -- that is, when something
                           # happens. The function "onkey" sets the screen to
                           # say "when the user presses this key, I want you to
                           # call this function." In English, the code
                           # screen.onkey(moveUp, "Up")
                           # translates into
                           # "When the user presses the Up ('"Up"') key while
                           # they are focused on the screen ('screen.'), call
                           # the function moveUp() ('moveUp'). It isn't totally
                           # necessary that you understand everything that is
                           # going on here, but I wanted to illustrate it so
                           # that, if you wanted to, you could explore this.
screen.onkey(moveDown, "Down")
screen.onkey(moveLeft, "Left")
screen.onkey(moveRight, "Right")
screen.listen() # We now want to tell the screen that it should listen for
                # events. That is, that we want it to actively care about the
                # keyboard and the mouse and things like that, and to do
                # something with them if we told them what to do. All we have
                # told the screen about was what to do with the arrow keys,
                # but it's entirely possible to have the screen pay attention
                # to a lot more.
screen.mainloop() # Now, we tell the screen that it should run. Basically, that
                  # until the user closes the window, the window should stay
                  # open and, if the user has the window focused, it will
                  # process the keyboard and "handle" the events that I
                  # told it to.

# If you have questions regarding something about this code, you're free to
# either contact me and ask about it, or to try removing a line of code that
# you're curious about from this program and run it to find out what is
# changed, and thereby what that line of code does for the program.
