<?php

if (isset($_POST["submit"]))
{

    $results = "<center><b>CS0008 Feedback survey</b><br />" . date("j F Y") . "</center><br /><br />";

    $results .= "<b>1.</b> Attended <b><u>" . (isset($_POST["q1"]) ? $_POST["q1"] : "an unspecified number of") . "</u></b> recitations.<br />";
    $results .= "<b>2.</b> Overall thoughts on the effectiveness of recitations: <b><u>" . $_POST["q2"] . "</u></b>.<br />";
    $results .= "<b>3.</b> Did they attend office hours? <b><u>" . (isset($_POST["q3"]) && $_POST["q3"] == "on" ? "Yes" : "No") . "</u></b>.<br />";
    if (isset($_POST["q3"]) && $_POST["q3"] == "on")
    {
        $results .= "<b>4.</b> They attended office hours <b><u>" . mb_strtolower($_POST["q4"]) . "</u></b>.<br />";
        $results .= "<b>5.</b> They thought that attending office hours <b><u>" . (isset($_POST["q5"]) && $_POST["q5"] == "on" ? "was" : "was not") .
            "</u></b> helpful.<br />";
    }
    else
    {
        $results .= "<b>4.</b> <i>Not applicable</i>.<br /><b>5.</b> <i>Not applicable</i>.<br />";
    }

    if (isset($_POST["q6"]) && $_POST["q6"] != "(Optional)")
    {
        $results .= "<br /><br /><b>6.</b> This person found the following to be helpful:<br />" . $_POST["q6"];
    }
    else
    {
        $results .= "<br /><br /><b>6.</b> <i>Omitted</i>.";
    }
    if (isset($_POST["q7"]) && $_POST["q7"] != "(Optional)")
    {
        $results .= "<br /><br /><b>7.</b> This person has the following suggestions:<br />" . $_POST["q7"];
    }
    else
    {
        $results .= "<br /><br /><b>7.</b> <i>Omitted</i>.";
    }

    $results .= "<br /><br /><b>8.</b> This student is a CS student? <b><u>" . (isset($_POST["q8"]) ? $_POST["q8"] : "unspecified") . "</u></b>.";
    
    $tries = 0;
    while ($tries < 5)
    {
        $tries++;
        if (mail("jbd19@pitt.edu", "CS0008 Feedback", $results, "From: jacob@deitloff.com\r\nContent-type: text/html;\r\nX-Mailer: PHP/" . phpversion()))
        {
            header("Location: http://www.deitloff.com/cs0008/");
            exit();
        }
    }

    exit("Couldn't send the e-mail! (I tried to multiple times)");
}

echo "<style>

td
{
    vertical-align:top;
    padding-bottom:20px;
}
td:first-child
{
    max-width:550px;
}

textarea
{
    width:550px;
    height:100px;
}

</style>";

echo "<script>

function validate()
{
    // q1
    var q1 = document.getElementsByName('q1');
    q1Answered = false;
    for (var index = 0; index < q1.length; index++)
    {
        if (q1[index].checked)
        {
            q1Answered = true;
            break;
        }
    }
    if (!q1Answered)
    {
        alert('Please answer question 1 before submitting your feedback.');
        return false;
    }

    // q2
    var q2 = document.getElementsByName('q2');
    q2Answered = false;
    for (var index = 0; index < q2.length; index++)
    {
        if (q2[index].checked)
        {
            q2Answered = true;
            break;
        }
    }
    if (!q2Answered)
    {
        alert('Please answer question 2 before submitting your feedback.');
        return false;
    }

    // q4
    if (document.getElementById('q3').checked)
    {
        var q4 = document.getElementsByName('q4');
        var q4Answered = false;
        for (var index = 0; index < q4.length; index++)
        {
            if (q4[index].checked)
            {
                q4Answered = true;
                break;
            }
        }
        if (!q4Answered)
        {
            alert('Please answer question 4 before submitting your feedback.');
            return false;
        }
    }

    // q8
    var q8 = document.getElementsByName('q8');
    var q8Answered = false;
    for (var index = 0; index < q8.length; index++)
    {
        if (q8[index].checked)
        {
            q8Answered = true;
            break;
        }
    }
    if (!q8Answered)
    {
        alert('Please answer question 8 before submitting your feedback.');
        return false;
    }
    
    
    // We're good!
    return true;
}

function q3Answered()
{
    if (document.getElementById('q3').checked)
    {
        document.getElementById('q45-skipped').style.display = 'none';
        document.getElementById('q4-row').style.display = 'table-row';
        document.getElementById('q5-row').style.display = 'table-row';
    }
    else
    {
        document.getElementById('q45-skipped').style.display = 'table-row';
        document.getElementById('q4-row').style.display = 'none';
        document.getElementById('q5-row').style.display = 'none';
    }
}

function q6Focus()
{
    if (document.getElementById('q6').style.color == 'gray')
    {
        document.getElementById('q6').style.color = 'black';
        document.getElementById('q6').value = '';
    }
}
function q6Blur()
{
    if (document.getElementById('q6').value == '')
    {
        document.getElementById('q6').value = '(Optional)';
        document.getElementById('q6').style.color = 'gray';
    }
}

function q7Focus()
{
    if (document.getElementById('q7').style.color == 'gray')
    {
        document.getElementById('q7').style.color = 'black';
        document.getElementById('q7').value = '';
    }
}
function q7Blur()
{
    if (document.getElementById('q7').value == '')
    {
        document.getElementById('q7').value = '(Optional)';
        document.getElementById('q7').style.color = 'gray';
    }
}

</script>\n";

echo "This form <b>isn't</b>:<br /><ul>";
echo "<li><b>Traced back to you.</b> Unless you sign your name (please don't), I won't know your name, your IP address, your student ID, " .
    "your information &mdash; anything. It's completely anonymous;</li>";
echo "<li><b>Going to affect your grade.</b> Not that I have much control over your grade to begin with;</li>\n";
echo "<li><b>Required.</b> I can't, and won't, force this out of you. It's entirely optional;</li>\n";
echo "<li><b>Going to kill my self-confidence.</b> I'm not doing this survey to only feel good about myself. I'm hoping to be a TA again, and I would really " .
    "like to improve where I need to.</li>\n";
echo "</ul>";
    
echo "<hr />";
echo "<form method=\"POST\" onsubmit=\"return validate();\">\n";
echo "<table><tr><td></td><td></td></tr>\n";

echo "<tr><td><b>1.</b> <i>Ignoring situations out of your control (death, hospitalization, presidential summons, etc)</i>, how many recitations did you attend?</td>";
echo "<td><input type=\"radio\" id=\"q1-all\" name=\"q1\" value=\"11-12\" /><label for=\"q1-all\">All (11&mdash;12)</label><br />" .
         "<input type=\"radio\" id=\"q1-most\" name=\"q1\" value=\"8-10\" /><label for=\"q1-most\">Most (8&mdash;10)</label><br />" .
         "<input type=\"radio\" id=\"q1-half\" name=\"q1\" value=\"5-7\" /><label for=\"q1-half\">Half (5&mdash;7)</label><br />" .
         "<input type=\"radio\" id=\"q1-few\" name=\"q1\" value=\"3-4\" /><label for=\"q1-few\">Few (3&mdash;4)</label><br />" .
         "<input type=\"radio\" id=\"q1-none\" name=\"q1\" value=\"0-2\" /><label for=\"q1-none\">None (0&mdash;2)</label></td></tr>";

echo "<tr><td><b>2.</b> In general, did you find recitations to be helpful?</td>";
echo "<td><input type=\"radio\" id=\"q2-yesVery\" name=\"q2\" value=\"Yes, very\" /><label for=\"q2-yesVery\">Yes, very</label><br />" .
         "<input type=\"radio\" id=\"q2-yesMostly\" name=\"q2\" value=\"Yes, mostly\" /><label for=\"q2-yesMostly\">Yes, mostly</label><br />" .
         "<input type=\"radio\" id=\"q2-sometimes\" name=\"q2\" value=\"Sometimes\" /><label for=\"q2-sometimes\">Sometimes</label><br />" .
         "<input type=\"radio\" id=\"q2-notReally\" name=\"q2\" value=\"No, not really\" /><label for=\"q2-notReally\">No, not really</label><br />" .
         "<input type=\"radio\" id=\"q2-rarely\" name=\"q2\" value=\"Rarely\" /><label for=\"q2-rarely\">Rarely</label><br />" .
         "<input type=\"radio\" id=\"q2-no\" name=\"q2\" value=\"No\" /><label for=\"q2-no\">Simply put, \"no\"</label></td></tr>";
         
echo "<tr><td><b>3.</b> Did you attend office hours?</td>";
echo "<td><input type=\"checkbox\" id=\"q3\" name=\"q3\" onclick=\"q3Answered()\" /><label for=\"q3\">Yes</label></td></tr>\n";

echo "<tr id=\"q45-skipped\"><td>Questions <b>4</b> and <b>5</b> pertain only to attendance of office hours.</td></tr>\n";

echo "<tr id=\"q4-row\" style=\"display:none;\"><td><b>4.</b> How often did you attend office hours?</td>";
echo "<td><input type=\"radio\" id=\"q4-often\" name=\"q4\" value=\"Often\" /><label for=\"q4-often\">Often</label><br />" .
         "<input type=\"radio\" id=\"q4-sometimes\" name=\"q4\" value=\"Sometimes\" /><label for=\"q4-sometimes\">Sometimes</label><br />" .
         "<input type=\"radio\" id=\"q4-onceTwice\" name=\"q4\" value=\"Once or twice\" /><label for=\"q4-onceTwice\">Once or twice</label></td></tr>\n";
     
echo "<tr id=\"q5-row\" style=\"display:none;\"><td><b>5.</b> Did you find that attending office hours were helpful?</td>";
echo "<td><input type=\"checkbox\" id=\"q5\" name=\"q5\" /><label for=\"q5\">Yes</label></td></tr>";
    
echo "</table>\n";

echo "<b>6.</b> Was there anything in particular that I did which you found to be helpful?<br />";
echo "<textarea name=\"q6\" id=\"q6\" style=\"color:gray;\" onfocus=\"q6Focus();\" onblur=\"q6Blur();\">(Optional)</textarea><br /><br />\n";

echo "<b>7.</b> During recitations, what would you suggest I change?<br />";
echo "<textarea name=\"q7\" id=\"q7\" style=\"color:gray;\" onfocus=\"q7Focus();\" onblur=\"q7Blur();\">(Optional)</textarea><br /><br />\n";

echo "<table><tr><td><b>8.</b> Are you a computer science student? <span style=\"color:gray;\">(Asking for no reason other than curiosity!)</span></td><td><input type=\"radio\" id=\"q8-major\" name=\"q8\" value=\"Computer Science major\" /><label for=\"q8-major\">Yes; computer science major</label><br /><input type=\"radio\" id=\"q8-minor\" name=\"q8\" value=\"Computer Science minor\" /><label for=\"q8-minor\">Yes; computer science minor</label><br /><input type=\"radio\" id=\"q8-nonCS\" name=\"q8\" value=\"Non-CS student\" /><label for=\"q8-nonCS\">No</label></td></tr></table>";


echo "<input type=\"submit\" value=\"Submit\" name=\"submit\" />\n";
echo "</form>\n";
?>