import math

class Money:
    def __init__(self, initialPrice):
        self.amount = initialPrice
        self.validate()

    def validate(self):
        if self.amount < 0:
            self.amount = 0.0
        else:
            self.amount = math.floor(self.amount * 100) / 100

    def __add__(self, other):
        if isinstance(other, Money):
            return Money(self.amount + other.amount)
        elif isinstance(other, (float, int)):
            return Money(self.amount + other)
        else:
            raise ValueError("Cannot add this value to money!")

    def __float__(self):
        return self.amount

    def __str__(self):
        return "$%.2f" % self.amount

class Product:
    def __init__(self, name, price):
        self.name = name
        self.price = price

    def getName(self):
        return self.name

    def getPrice(self):
        return self.price

    def __str__(self):
        return "%s (%s)" % (self.name, self.price)

milk = Product("Milk", Money(2.39))
eggs = Product("Eggs", Money(3.40))
wallet = Money(5.00)

print(milk)
print(eggs)

print(wallet)
wallet = wallet + milk.getPrice()
print(wallet)

wallet = wallet + 4.27
print(wallet)
