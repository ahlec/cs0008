class Person:
    def __init__(self, firstName, lastName):
        self.first = firstName
        self.last = lastName
        self.knows = []

    def introduceTo(self, otherPerson):
        self.knows.append(otherPerson)

    def doesKnow(self, otherPerson):
        for person in self.knows:
            if person == otherPerson:
                return True
        return False

    def __str__(self):
        return self.first + " " + self.last

bob = Person("Bob", "McMan")
joe = Person("Joe", "Someone")
bob.introduceTo(joe)

if bob.doesKnow(joe):
    print("knows")
else:
    print("doesn't know")

if joe.doesKnow(bob):
    print("knows")
else:
    print("doesn't know")
