An <kw>if</kw> statement, when we're putting in our <term>conditional</term> (eg, <python>if <u>myMajor == "Computer Science"</u>:</python>), needs that conditional to <term>evaluate</term> (result in; ultimately become) to a boolean &mdash; either <kw>True</kw> or <kw>False</kw>. If the conditional evaluates to <kw>True</kw>, then the <kw>if</kw> statement goes through with the code you told it to do; if the conditional evaluates to <kw>False</kw>, then the <kw>if</kw> statement skips over the code you told it to do, either executing code you wrote in an <kw>else</kw> clause, or doing nothing if you didn't have an <kw>else</kw> clause.

<emphasis>"But wait, what about&mdash;"</emphasis>

Let's look at what we've done in the past. Every time we've used <python>==</python> before, we're "transforming" some piece of data into a boolean. The same way that, if we had an integer variable <python>myAge</python> and wanted to turn it into a string we would write <python>str(myAge)</python>, when we have something that isn't a boolean (isn't <kw>True</kw> or <kw>False</kw>), if we want to "use" it as a boolean, we need to "turn it into one." Hence why we've used <python>==</python> in the past. As we saw when we looked at our function <python>isEqual(x, y)</python>, we expect back either the value <kw>True</kw> or the value <kw>False</kw>. The same goes for numbers. Would it make sense for us to write <python><kw>if</kw> 17:</python>? <emphasis>"If seventeen what?"</emphasis>

So why can't I write <python><kw>if</kw> isEqual(myMajor, "Computer Science") == <kw>True</kw>:</python>? Well, you can. It works fine. But why would you want to do this? Refer to this code snippet:

<code>set(myMajor, "Computer Science")
<kw>if</kw> isEqual(myMajor, "Computer Science") == True:
    print("Cool! Enjoy!")</code>
    
When we get to our <kw>if</kw> statement, we first <term>call</term> (execute) the function <python>isEqual(myMajor, "Computer Science")</python>. It returns <kw>True</kw>, right? The two values are equal. So, if we could free Python at that moment in time &mdash; as soon as we were done with <python>isEqual(myMajor, "Computer Science")</python>, wouldn't our code "look" something like this?

<code><kw>if True</kw> == <kw>True</kw>:
    print("Cool! Enjoy!")</code>
    
Because <python>isEqual(myMajor, "Computer Science")</python> evaluates to <kw>True</kw>. Doesn't this line of code look a bit strange, though? Well of course <kw>True</kw> equals <kw>True</kw>. It looks odd to us now because we can see it now how the computer sees it. If what "powers" an <kw>if</kw> statement is that boolean conditional we talked about, it seems a bit odd to try to turn a boolean into a boolean. It's already a boolean!