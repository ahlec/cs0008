Let's use a bit more of a practical example. In assignment 3, we had a function <python>isRobotFacingRight()</python> which returned <kw>True</kw> or <kw>False</kw>, depending on whether the robot was facing right or not.

So let's make a new function (that wasn't <emphasis>exactly</emphasis> in the assignment, so don't freak out). This function will make the robot take a step forward in whichever direction he is currently facing. As we know, if he is facing right, his <python>position</python> should increase by 1. If he is facing left, it should decrease by 1. Let's look at the code for this function:

<code><kw>def</kw> <blue>moveForward</blue>():
    <kw>global</kw> position
    <kw>if</kw> isRobotFacingRight():
        position = position + 1     <comment># increase his position by 1</comment>
    <kw>else</kw>:
        position = position - 1     <comment># decrease his position by 1</comment></code>
        
Hopefully this makes sense? <python>isRobotFacingRight()</python> returns either <kw>True</kw> or <kw>False</kw> and the <kw>if</kw> conditional takes a <kw>True</kw> or <kw>False</kw>, so we're just connecting the dots.

<note>We <emphasis>could</emphasis> write <python><kw>if</kw> isRobotFacingRight() == <kw>True</kw>:</python>, but as we saw on the previous page, that would be overkill, wouldn't it?</note>

Let's return to <python>moveForward()</python>. As you all remember, the robot can only move within a certain area. At all times, the <python>position</python> must be within 0 and <python><func>len</func>(track) - 1</python>. We had another function <python>isBlocked()</python> that would return <kw>True</kw> if the robot could not move forward in his current direction, or <kw>False</kw> if he could. We also had another function <python>changeDirection()</python> that would make the robot his direction. Let's change <python>moveForward()</python> so that, before the robot makes a step forward, he checks if he is blocked; if he is, he changes direction. See if you can't write this out, then check the next page for the solution.