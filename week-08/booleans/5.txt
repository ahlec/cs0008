Because we said that, in all cases, <python>set(x, y)</python> was equal to writing <python>x = y</python>, and that <python>isEqual(x, y)</python> was exactly the same as <python>x == y</python>, then shouldn't these two blocks of code be <emphasis>exactly</emphasis> the same?

<code>myMajor = "Computer Science"
<kw>if</kw> myMajor == "Computer Science":
    print("Cool! I hope you enjoy it!")</code>
    
<code>set(myMajor, "Computer Science")
<kw>if</kw> isEqual(myMajor, "Computer Science"):
    print("Cool! I hope you enjoy it!")</code>
    
They should be equal, because all we did was replace things with things that were <emphasis>exactly</emphasis> the same.

I'm assuming, though, that there are suddenly some objections to what I've just written. <emphasis>"But Jacob, how is this the case? <python>if isEqual(myMajor, "Computer Science"):</python> isn't comparing something. I mean, sure, it logically makes sense, but don't we need to compare it to something? Like perhaps, <python>if isEqual(myMajor, "Computer Science") == True:</python>?"</emphasis>

And here is where I believe is a fundamental misunderstanding. The <kw>if</kw> statement isn't tied directly to such things as <python>==</python> or <python>&gt;</python> or <python>&lt;=</python>. Yes, they're very common to use, and are invaluable. But the <kw>if</kw> statement doesn't require that at least one <emphasis>must</emphasis> be used. Rather, remember what I said an <kw>if</kw> statement sounds like in English? "If this is the case, do this; otherwise, do this other thing?" Instead of being tied directly to some comparison <term>operator</term> like <python>==</python>, an <kw>if</kw> statement <emphasis>is</emphasis> tied directly to <strong>booleans</strong>.