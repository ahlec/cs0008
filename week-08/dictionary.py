dictionary = open("dictionary.txt", "r")

longestWord = None
longestWordLength = 0

for line in dictionary:
    word = line.rstrip("\n")
    if len(word) > longestWordLength:
        longestWord = word
        longestWordLength = len(word)
        print("New longest word: '%s' (%d)" % (longestWord, longestWordLength))

dictionary.close()

if longestWord != None:
    print("\nLongest word: '%s' (%d)" % (longestWord, longestWordLength))
