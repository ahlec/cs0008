total = 0
numberCount = 0

numbers = open("numbers.txt", "r")
for line in numbers:
    number = int(line.rstrip("\n"))
    total = total + number
    numberCount = numberCount + 1

print("%d numbers counted." % numberCount)
print("%d (total sum)." % total)
print("%.2f (average)" % (total / numberCount))

numbers.close()
