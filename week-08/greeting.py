myName = input("What is your name? ")

namesFile = open("names.txt", "r")
foundName = False
for name in namesFile:
    if name.rstrip("\n") == myName:
        print("Welcome back, %s!" % myName)
        foundName = True
        break
namesFile.close()

if foundName == False:
    print("I haven't met you before. Let me write your name down.")
    namesFile = open("names.txt", "a")
    namesFile.write(myName + "\n")
    namesFile.close()
    print("Alright, %s. I'll see you again later!" % myName)
