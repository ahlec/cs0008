# Below is code to make the turtle draw polygons of any number of sides. This
# program will continue to draw polygons of random number of sides (between
# 3 and 23) continually.
#
# Your job is to create the function calculateAngle() that drawPolygon
# uses. Your function takes the number of sides of the polygon, and needs to
# return the angle that the turtle must turn after drawing each side.
import turtle
import random

# Put your code here!

# Do not modify below this line!
def drawPolygon(numberSides, sideLength):
    angle = calculateAngle(numberSides)
    for index in range(numberSides):
        turtle.forward(sideLength)
        turtle.left(angle)

while True:
    numberSides = int(random.random() * 20 + 3)
    turtle.Screen().title("Number of sides: " + str(numberSides)) # Change the
                                                                  # window title
    drawPolygon(numberSides, 50)
    turtle.clear()
turtle.done()