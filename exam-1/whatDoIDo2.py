def count(s):
    n1 = 0
    for L in s:
        if L == 'a' or L == 'e' or L == 'i' or L == 'o' or L == 'u' or L == 'y':
            n1 = n1 + 1
    return n1

print("university: %d" % count("university"))
print("of: %d" % count("of"))
print("pittsburgh: %d" % count("pittsburgh"))