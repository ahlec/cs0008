def a(x):
    if x == 10:
        return 5
    else:
        return 1

def b(w):
    if w == 7:
        return 9
    else:
        return 4

def c(t):
    if t == 3:
        return 6
    else:
        return 2

def d(p):
    if p == 5:
        return 3
    else:
        return 10

def e(g):
    if g == 2:
        return 8
    else:
        return g + 1

s = a(10)
s = d(s)
s = c(s)
s = e(s)
s = b(s)

if s == 9:
    print("Unlocked")
else:
    print("Locked")
