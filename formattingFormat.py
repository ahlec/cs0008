# You can do some pretty interesting things with format(). Because it returns
# a string, you can use that string for a lot of things... include as a
# format string to use in format() again. Look at this example:

number = int(input("Please enter a number: "))
spaces = int(input("Please enter the number of spaces for this column: "))

formattingCode = ("%%%dd" % spaces)
print(formattingCode)
print(formattingCode % number)

# This isn't on the test, and this won't ever come up in class -- and it's
# actually horrible coding, to do things this way. But I thought it was kind of
# cool, and also lets you see how you can fit blocks and pieces together and
# create new things. If you know what something gives you back (returns) and
# what something else asks for (arguments), try putting them together and see
# what you can do!
