<?php

error_reporting(E_ALL);
ini_set("display_errors", "1");

$selectedWeek = null;
if (isset($_GET["path"]))
{
    $path = explode("/", $_GET["path"]);
    $selectedWeek = $path[0];
}

$weeks = array();
$weekDirs = scandir(getcwd());
foreach ($weekDirs as $week)
{
    if ($week == "." || $week == ".." || $week == "files")
    {
        continue;
    }
    
    if (is_dir($week))
    {
        $weeks[] = $week;
    }
}

/*$directories = array();

$files = scandir(getcwd());
foreach ($files as $file)
{
    if ($file == "." || $file == ".." || $file == "files")
    {
        continue;
    }
    
    if (is_dir($file))
    {
        $directories[] = $file;
    }
}*/

$section = null;
$page = null;
if (!in_array($selectedWeek, $weeks))
{
    $selectedWeek = null;
}
else if (isset($path) && isset($path[1]))
{
    $weekSections = scandir(getcwd() . "/" . $selectedWeek);
    if ($weekSections !== false)
    {
        foreach ($weekSections as $weekSection)
        {
            if ($weekSection == "." || $weekSection == ".." || $weekSection == "files")
            {
                continue;
            }

            if (is_dir(getcwd() . "/" . $selectedWeek . "/" . $weekSection) && $weekSection == $path[1])
            {
                $section = $path[1];
                break;
            }
        }
    }

    if ($selectedWeek != null && $section == null)
    {
        $selectedWeek = null;
    }
    else
    {
        if (!isset($path[2]))
        {
            $page = 1;
        }
        else
        {
            $page = $path[2];
        }
    
        if (!ctype_digit($page))
        {
            $page = null;
        }
        else if (!file_exists($selectedWeek . "/" . $section . "/" . $page . ".txt"))
        {
            $page = null;
        }
    }
    if ($section != null && $page == null)
    {
        $selectedWeek = null;
    }
}

echo "<html>";
echo "<head>";
echo "<title>CS0008: Introduction to Computer Programming with Python</title>\n";
echo "<link rel=\"stylesheet\" href=\"http://www.deitloff.com/cs0008/style.css\" />\n";
echo "<base href=\"http://www.deitloff.com/cs0008/\"/>\n";
echo "</head>";
echo "<body>";
if (mb_strtolower($path[0]) == "feedback")
{
    require_once("feedback.php");
}
else if ($selectedWeek == null)
{
    foreach ($weeks as $week)
    {
        echo "<h1>" . preg_replace("/-0?/", " ", $week) . "</h1>\n";
        if (file_exists($week . "/index.htm"))
        {
            echo "<div class=\"week\"><a href=\"" . $week . "/index.htm\">Access</a></div>\n";
            continue;
        }

        echo "<div class=\"week\">\n";
        $weekFiles = scandir(getcwd() . "/" . $week);
        if ($weekFiles !== false)
        {
            foreach ($weekFiles as $files)
            {
                if ($files == "." || $files == ".." || $files == "files" || !is_dir($week . "/" . $files))
                {
                    continue;
                }
                
                echo "<a href=\"" . $week . "/" . $files . "/1\" class=\"topic\">" . $files . "</a>";
            }
            
            
            $fileDir = scandir(getcwd() . "/" . $week . "");
            if ($fileDir !== false)
            {
                foreach ($fileDir as $file)
                {
                    if ($file == "." || $file == ".." || is_dir($week . "/" . $file))
                    {
                        continue;
                    }
                    
                    echo "<a href=\"" . $week . "/" . $file . "\" class=\"file " . pathinfo($week . "/" . $file, PATHINFO_EXTENSION) . "\">" . $file . "</a>\n";
                }
            }
        }
        else
        {
            echo "Nothing yet!";
        }
        echo "</div>\n";
    }
    /*echo "<h1>Topics</h1>\n";
    foreach ($directories as $directory)
    {
        echo "<a href=\"" . $directory . "/1\" class=\"topic\">" . $directory . "</a>";
    }
    
    $recitationFiles = array();
    $files = scandir("files");
    foreach ($files as $file)
    {
        if ($file == "." || $file == "..")
        {
            continue;
        }
        
        $recitationFiles[] = $file;
    }
    
    if (count($recitationFiles) > 0)
    {
        echo "<h1>Files</h1>\n";
        foreach ($recitationFiles as $file)
        {
            echo "<a href=\"files/" . $file . "\" class=\"file\">" . $file . "</a>\n";
        }
    }*/
}
else
{
    echo "<script>window.onkeydown = function(e)
    {
        if (!e)
        {
            var e = window.event;
        }
        if (e.keyCode == 39)
        {
            window.location = " . (file_exists($selectedWeek . "/" . $section . "/" . ($page + 1) . ".txt") ?
                "'" . $selectedWeek . "/" . $section . "/" . ($page + 1) . "/'" : "''") . ";
        }
        else if (e.keyCode == 37 && " . $page . " > 1)
        {
            window.location = '" . $selectedWeek . "/" . $section . "/" . ($page - 1) . "/';
        }
    };</script>";
    echo "<a href=\"\" class=\"home\">Return home</a>";
    echo "<h1>Topic: " . $section . "</h1>\n";
    
    echo "<div class=\"page\">" . file_get_contents($selectedWeek . "/" . $section . "/" . $page . ".txt") . "<div class=\"done\"></div>";
    
    if ($page > 1)
    {
        echo "<a href=\"" . $selectedWeek . "/" . $section . "/" . ($page - 1) . "\" class=\"previous\">Previous page</a>";
    }
    
    if (file_exists($selectedWeek . "/" . $section . "/" . ($page + 1) . ".txt"))
    {
        echo "<a href=\"" . $selectedWeek . "/" . $section . "/" . ($page + 1) . "\" class=\"next\">Next page</a>";
    }
    else
    {
        echo "<a href=\"\" class=\"finish\">Finish</a>";
    }
    echo "</div>";
}
?>