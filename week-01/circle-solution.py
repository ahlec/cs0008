# CS0008 Recitation, Week 01: Additional Problem
#
# PROBLEM:
#   Ask the user to provide the radius of a circle, and then print
#   the diameter of the circle, the circumference of the circle, and
#   the area of the circle.
# FORMULAS (review):
#   * Diameter = 2 * radius
#   * Circumference = 2 * pi * radius
#   * Area  = pi * radius ^ 2
#           = pi * radius * radius
# RULES:
#   *   You cannot write out 3.14 in your calculations. You must use the math
#       library here.
#   *   You cannot simply multiply a number by itself. You must use a function
#       that puts a number to an exponent. The math library has one such
#       function.
#   *   print() each value we calculate on a different line.
#   *   When we print() out the values, round them to two decimal places.
# SOME HINTS:
#   *   You will need to import the math library.
#   *   There is a built-in function (we don't need to import anything) for
#       rounding.

import math

radius = int(input("What is the radius of the circle? "))

# diameter
diameter = 2 * radius
print("Diameter:", round(diameter, 2))

# circumference
circumference = 2 * math.pi * radius
print("Circumference:", round(circumference, 2))

# area = pi * r^2
area = math.pi * math.pow(radius, 2)
# alternatively, "pow()" is a built-in function, and so we could write
#   area = math.pi * pow(radius, 2)
# and receive the same results as the above line.
# Not all functions are built-in, but for a host of different reasons,
# pow() *has* been, and functions as we expect it to for numbers.
print("Area:", round(area, 2))
