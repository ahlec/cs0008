# CS0008 Recitation, Week 01: Additional Problem
#
# PROBLEM:
#   Ask the user for four input values: X1, Y1, X2, and Y2. Then, calculate
#   and output the slope between these two points. Next, ask for the
#   y-intercept of the line, and output the simple line formula using the
#   calculated slope and y-intercept. Finally, Ask the user for an X value,
#   and use the above values to calculate and print() the Y value for the
#   provided X value.
# FORMULAS (review):
#   * m (slope) = Y2 - Y1
#                 -------
#                 X2 - X1
#   * b (y-intercept)
#   * simple line formula:
#       y = m*x + b
# RULES:
#   *   You must calculate the slope in one line of code, using only the
#       input variables: X1, Y1, X2, and Y2.
#   *   When printing the line formula, you must use only one print()
#       statement, and you are required to use , (instead of +). Furthermore,
#       the output must be in the form:
#           "Line formula: y = (SLOPE)*x + (Y-INTERCEPT)
#       Note: despite using , and only one print() statement, the output must
#           not have spaces between the parentheses and the variables.
#   *   All output variables must be rounded to three decimal places.
# SOME HINTS:
#   *   You should not (necessarily) need the math library.
#   *   Because slope is usually a fractional number, use decimal numbers
#       instead of integer numbers for calculations.
#   *   There is a built-in function for rounding.
#   *   It is possible to change the default separator that print() puts
#       between two comma separated items.
#   *   When calculating slope, remember order of operations. A graphing
#       calculator is no different than a computer here: how would you do
#       this in one line on a graphing calculator?

x1 = float(input("X1? "))
y1 = float(input("Y1? "))
x2 = float(input("X2? "))
y2 = float(input("Y2? "))

slope = (y2 - y1) / (x2 - x1)
# Order of operations is key for this problem! And luckily (and not unintentionally), order of operations
# for arthimetic in python are the same as they are in "normal" math. When doing Python math, it is usually
# safe to go at it from a mathematical point of view -- especially for these more fundamental arithmetic
# operations, there isn't a different "computer science" way of doing mathematics.

print("The slope is:", round(slope, 3))

b = round(float(input("Y-intercept? ")), 3)
# We can put multiple functions on the same line -- as many as we want, though if we put too
# many on one line, it can become difficult to read. At the same time, if we were to draw something
# conceptually simple (such as, say, performing the arithmetic "x = 2 * 9 - 1 / 6 + 1" on six different
# lines of code), it can make it difficult to read. Every person has a different "balance" for when
# to put multiple functions on the same line and when to split them into multiple functions. I put these
# three together because, for me, I knew that I wanted to ask ("input()") the user for a decimal
# number ("float()"), rounded to three decimal places ("round()"). Don't be afraid to break up the code onto
# multiple lines, however.

print("\nLine formula: y = (", slope, ")*x + (", b, ")", sep="")
# \n is an escape character for a newline. By using the newline escape character,
# I am explicitly telling Python to print everything following the escape character
# onto the next line. Above, I put it at the start of the print() statement to put
# an empty line between the input "Y-intercept? " prompt, and the output of the formula.
# Running the program should demonstrate what I mean by this; try removing the \n from the
# start of the string and see what changes.

x = float(input("X? "))
y = round(x * slope + b, 3)
print("Y:", y)
