# CS0008 Recitation, Week 01: Additional Problem
#
# PROBLEM:
#   Ask the user for four input values: X1, Y1, X2, and Y2. Then, calculate
#   and output the slope between these two points. Next, ask for the
#   y-intercept of the line, and output the simple line formula using the
#   calculated slope and y-intercept. Finally, Ask the user for an X value,
#   and use the above values to calculate and print() the Y value for the
#   provided X value.
# FORMULAS (review):
#   * m (slope) = Y2 - Y1
#                 -------
#                 X2 - X1
#   * b (y-intercept)
#   * simple line formula:
#       y = m*x + b
# RULES:
#   *   You must calculate the slope in one line of code, using only the
#       input variables: X1, Y1, X2, and Y2.
#   *   When printing the line formula, you must use only one print()
#       statement, and you are required to use , (instead of +). Furthermore,
#       the output must be in the form:
#           "Line formula: y = (SLOPE)*x + (Y-INTERCEPT)
#       Note: despite using , and only one print() statement, the output must
#           not have spaces between the parentheses and the variables.
#   *   All output variables must be rounded to three decimal places.
# SOME HINTS:
#   *   You should not (necessarily) need the math library.
#   *   Because slope is usually a fractional number, use decimal numbers
#       instead of integer numbers for calculations.
#   *   There is a built-in function for rounding.
#   *   It is possible to change the default separator that print() puts
#       between two comma separated items.
#   *   When calculating slope, remember order of operations. A graphing
#       calculator is no different than a computer here: how would you do
#       this in one line on a graphing calculator?