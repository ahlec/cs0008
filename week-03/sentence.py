# CS0008 Recitation, Week 03: Additional Problem
#
# PROBLEM:
#   Construct a sentence from the individual words. Prompt the user for
#   words, and then, if the latest input is not a period (.), exclamation
#   mark (!), or question mark(?), add it (and a preceeding space) to the
#   current sentence; if it is one of the aforementioned punctuation marks,
#   simply tack on the punctuation mark to the end of the sentence, and then
#   print out the sentence in quotation marks.
# RULES:
#   *   You cannot have any extraneous spaces between words -- you can't
#       have a space character at the start of the sentence, a space
#       character before the punctuation mark, or more than one space between
#       words (unless the user has typed a space as input).
#           * You shouldn't have additional spaces as a result of your code.
#   *   Your program must continue to prompt for additional words forever,
#       stopping only if the input is one of the three punctuation marks.
#   *   If the very first input is a punctuation mark, your program must
#       acknowledge it, and properly deal with the sentence of just that
#       punctuation mark.
# SOME HINTS:
#   *   There is a trick to make it easy to have a loop act a certain way
#       during the first iteration and then differently on subsequent
#       loops. See the flowchart found in the week-03 directory for a
#       graphical representation.