# CS0008 Recitation, Week 03: Additional Problem
#
# PROBLEM:
#   Repeatedly ask the user for a number. If that number is a prime number,
#   print out "Unlocked!" and allow the program to end; if the number is not
#   a prime number, print out "INVALID. TRY AGAIN." and ask again.
# FORMULAS:
#   * A prime number is a number that is greater than or equal to 2 and which
#     is divisible only by 1 and itself.
# RULES:
#   *   You cannot use modulus (%) for determining if a number is prime.
#   *   Your program must continue to ask the user forever until they enter
#       a prime number.
#   *   You must make use of a while loop, a for loop, and (at least) one
#       if statement.
# SOME HINTS:
#   *   Consider what we get when we divide a number by a number it is
#       divisible by. 4 / 2 = 2.0 while 5 / 2 = 2.5. Consider the two types
#       of division that can be performed in Python, and when the two will
#       return the same result, and what that might mean.
#   *   for loops which make use of range() are able to specify both with
#       what number they begin as well as with what number they end immediately
#       before (remember: the upper bound of range() is not inclusive!)
#   *   Statements like break and continue will only work within the loop
#       they are immediately inside; if we are, say, inside a loop within a
#       loop (wink wink) and we want to leave both of them, we would need to
#       first break from the first loop, and then have a way of knowing that
#       we should break from the second loop.
#           * Variables are your friend!
#           * There is no need to go deeper ("loop within a loop" is as far
#             as you should need to go, Mr. Cobb)
while True:
    userInput = int(input("Enter a number: "))
    if userInput > 1:
        isPrime = True
        for divisor in range(2, userInput):
            if userInput // divisor == userInput / divisor:
                isPrime = False
                break
        if isPrime:
            break
    print("INVALID. TRY AGAIN.")
print("Unlocked!")
