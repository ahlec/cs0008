# CS0008 Recitation, Week 03: Additional Problem
#
# PROBLEM:
#   Create a simple calculator! Creating a loop, ask the user for an operation.
#   Determine what operation the user wants:
#
#       input     |  operation
#       ----------+-----------
#       add       |      +
#       plus      |      +
#       +         |      +
#       minus     |      -
#       subtract  |      -
#       -         |      -
#       multiply  |      *
#       times     |      *
#       *         |      *
#       divide    |      /
#       divided   |      /
#       /         |      /
#       quit      |      quit
#       q         |      q
#
#   If the user typed any other input for operation, print out "Invalid
#   Operation!" and then print "Goodbye!" and allow the program to end.
#   If the user typed "quit" or "q" then print "Goodbye!" and allow the
#   program to end.
#
#   If the user typed an operation that translated to +, -, *, or /,
#   ask the user for two numbers (a and b). Then, depending on the operation
#   the user asked for, add, subtract, multiply, or divide the two numbers,
#   and then print out the final calculation in the form:
#
#       a (+, -, *, /) b = result
# SOME HINTS:
#   *   It sounds a great deal more challenging than it actually is, I
#       promise.
#   *   Try using elif statements. The format for these, as a review, are:
#           if a == 1:
#               # do something because a = 1
#           elif a > 1 and a < 9:
#               # do something because 1 < a < 9
#           else:
#               # do something because a >= 9
#
#       You can have as many elif statements as you want on a single if
#       statement, but all of the elif statements must come before the
#       final else statement (which you are not required to have -- the
#       only part ever required for an if statement is the if statement itself)
#   *   To be helpful, a flowchart of the entire program has been provided in
#       the week-03 directory. I would reference that as you go along through
#       this exercise.
#           * Don't overthink it too much, especially at the start. I made
#             the flowchart very, very close to what my solution was in terms
#             of code (the only difference being that I didn't use Python
#             code directly on the flowchart).
while True:
    operationInput = input("operation? ")
    if operationInput == "plus" or operationInput == "add" or \
       operationInput == "+":
        operator = "+"
    elif operationInput == "minus" or operationInput == "subtract" or \
         operationInput == "-":
        operator = "-"
    elif operationInput == "multiply" or operationInput == "times" or \
         operationInput == "*":
        operator = "*"
    elif operationInput == "divide" or operationInput == "divided" or \
         operationInput == "/":
        operator = "/"
    elif operationInput == "quit" or operationInput == "q":
        break
    else:
        print("Invalid operation!")
        break
    
    number1 = int(input("a? "))
    number2 = int(input("b? "))

    if operator == "+":
        result = number1 + number2
    elif operator == "-":
        result = number1 - number2
    elif operator == "*":
        result = number1 * number2
    elif operator == "/":
        result = number1 / number2

    print(number1, operator, number2, "=", result)
    
print("Goodbye!")
