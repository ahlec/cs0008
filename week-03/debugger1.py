# This program is supposed to sum the squares of the consecutive integers
# between the inputs a and b inclusive (note: make sure a <= b). For example, if a = 3 
# and b = 5, the sum 3^2 + 4^2 + 5^2 = 50. Make sure you test your program on more than 
# one or two sets of inputs.

# Program 1
a = int(input("a: "))
b = int(input("b: "))
sum = 0
k = b
while k>=a:
    sum = sum + k*k
    k = k + 1

print(sum)