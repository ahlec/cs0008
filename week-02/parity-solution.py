# CS0008 Recitation, Week 02: Additional Problem
#
# PROBLEM:
#   Ask the user for an integer, and then tell the user the parity (whether
#   the number is even or odd) of that number.
# FORMULAS:
#   * Consider even numbers and odd numbers. If a number is even, we know
#     that we can divide by two (2) and get a whole number; if a number is
#     odd, we know that if we divide by two, we get a fractional number.
# RULES:
#   *   You cannot put a print statement inside of an if-else statement.
#   *   Output must be in the format of "(NUMBER) is even." or
#       "(NUMBER) is odd."
#   *   You may only use one print statement.
#   *   You cannot use the concatenation operator (+) or the comma (,) for
#       putting together your print statement.
# SOME HINTS:
#   *   Think about mathematical operators that you've learned in Python that
#       you don't use for everyday math.
#   *   You can define variables (or change their value) inside of if-else
#       statements.
#   *   If you want to provide multiple values to a print format statement,
#       put each variable inside of a tuple. Without getting to far ahead,
#       think of a tuple as just a list of information. If you put information
#       in a tuple, you surround it with parentheses and break each value with
#       a comma. Let me demonstrate how you would print() out a formatted
#       statement with two pieces of information instead of just one:
#
#           print("My name is %s %s." % ("Jacob", "Deitloff"))
#
#       As we've learned, %s will put the string (text) in the proper location
#       when you're printing out; what I've done is provided it with two
#       strings to print out. This might look (and sound) confusing at first,
#       and if so, it's just a challenge; this isn't a requirement, so don't
#       worry. Just as another example (hint, hint):
#
#           print("%s is %d years old." % ("Alec", 20))
#
#       This will print out "Alec is 20 years old."

inputNumber = int(input("Please input a number: "))

if inputNumber % 2 == 0:
    parity = "even"
else:
    parity = "odd"

print ("%d is %s." % (inputNumber, parity))
